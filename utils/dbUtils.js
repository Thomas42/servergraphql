const Sequelize = require('sequelize');

class DbUtils {

    /*
        init connection to MySQL DB with Sequelize
        init sequelize model :
            * Task
            * User
     */
    constructor() {
        // Config MySQL connection
        this.env = process.env.NODE_ENV || 'development'
        this.config = require('../config')[this.env]
        this.sequelize = new Sequelize(this.config.database.db, this.config.database.user, this.config.database.password, {
            host: this.config.database.host,
            port: this.config.database.port,
            dialect: 'mysql',

            pool: {
                max: 5,
                min: 0,
                acquire: 30000,
                idle: 10000
            }
        })


        // Task sequelize model
        this.Task = this.sequelize.define('task', {
            id: {
                type: Sequelize.STRING,
                primaryKey: true
            },
            name: Sequelize.STRING,
            date: Sequelize.DATE
        }, {
            timestamps: false
        })

        // User sequelize model
        this.User = this.sequelize.define('user', {
            id: {
                type: Sequelize.STRING,
                primaryKey: true
            },
            username: Sequelize.STRING
        })

        // Create table if not exist
        this.User.sync()

        // Set relations between Users / Tasks
        this.User.Tasks = this.User.hasMany(this.Task)
        this.Task.User = this.Task.belongsTo(this.User);

        // Create table if not exist
        this.Task.sync()



    }

    // Just for test connection
    test() {
        this.sequelize
            .authenticate()
            .then(() => {
                console.log('Connection has been established successfully.');
            })
            .catch(err => {
                console.error('Unable to connect to the database:', err);
            });
    }
}

module.exports = DbUtils;