# NodeJS GraphQL Server
Demo GraphQL in NodeJS server

Todo List

## Install
* `git clone https://github.com/thomascombe/TodoListGraphQLServer.git`
* `npm install` 
* Create `config.js` like `config.default.js` template with your informations (database, server port)
* Install GulpJS : `npm install -g gulp`

## Run
`gulp start`  
Server will be launch and tables create
  
Server is auto restart when your modify it.

Go to : `http://localhost:[port]/graphql`


## Examples

**Request (New User):**  
```
mutation {
  newUser(user: {
    username: "Thomas"
  }) {
    id
  }
}
```  
**Results**
```json
{
  "data": {
    "newUser": {
      "id": "fe575707-1a1e-4b06-9d2b-0345f620026f"
    }
  }
}
```
**Request (New Task):**  
```
mutation {
  newTask(task: {
    name: "Cooking",
    date: "2017-11-28"
    userId: "fe575707-1a1e-4b06-9d2b-0345f620026f"
  }) {
    id
  }
}
```
**Results :**   
New task in database  
```json
{
  "data": {
    "newTask": {
      "id": "8ba5fd35-950d-43cc-8a61-af3982990684"
    }
  }
}
```

---

**Request (Get all Tasks with some fields):**  
```json
query {
  tasks {
    id,
    name,
    user {
      id,
      username
    }
  }
}
```
**Results :**   
```json
{
  "data": {
    "tasks": [
      {
        "id": "8ba5fd35-950d-43cc-8a61-af3982990684",
        "name": "Cooking",
        "user": {
          "id": "fe575707-1a1e-4b06-9d2b-0345f620026f",
          "username": "Thomas"
        }
      },
      {
        "id": "f6ae8f02-affd-4132-9883-40ba11630f9d",
        "name": "Clean kitchen",
        "user": {
          "id": "fe575707-1a1e-4b06-9d2b-0345f620026f",
          "username": "Thomas"
        }
      }
    ]
  }
}
```