const {
    GraphQLString,
    GraphQLObjectType,
    GraphQLNonNull,
    GraphQLInputObjectType,
    GraphQLList
} = require('graphql');

const resolver = require("graphql-sequelize");

// get db utils
let DBUtils = require('../utils/dbUtils')
let db = new DBUtils()

// Task GraphQL Object
const TaskType = new GraphQLObjectType({
    name: "Task",
    description: "This represent a Task in GraphQL",
    fields: () => ({
        id: {type: new GraphQLNonNull(GraphQLString)},
        name: {type: new GraphQLNonNull(GraphQLString)},
        date: {type: new GraphQLNonNull(GraphQLString)},
        user: {
            type: UserType,
            resolve: resolver.resolver(db.Task.User)
        }
    })
});

// Task input for mutation
const TaskInputType = new GraphQLInputObjectType({
    name: 'TaskInput',
    fields: () => ({
        name:        { type: new GraphQLNonNull(GraphQLString) },
        date:        { type: new GraphQLNonNull(GraphQLString) },
        userId:      { type: new GraphQLNonNull(GraphQLString) }
    })
});

// User GraphQL Object
const UserType = new GraphQLObjectType({
    name: "User",
    description: "This represent a User in GraphQL",
    fields: () => ({
        id: {type: new GraphQLNonNull(GraphQLString)},
        username: {type: new GraphQLNonNull(GraphQLString)},
        tasks: {
            type: new GraphQLList(TaskType),
            resolve: resolver.resolver(db.User.Tasks)
        }
    })
});

// User input for mutation
const UserInputType = new GraphQLInputObjectType({
    name: 'UserInput',
    fields: () => ({
        username:   { type: new GraphQLNonNull(GraphQLString) }
    })
});

module.exports = {
    TaskType: TaskType,
    TaskInputType: TaskInputType,
    UserType: UserType,
    UserInputType: UserInputType
}