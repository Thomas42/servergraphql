const express = require("express"),
        resolver = require("graphql-sequelize"),
        bodyParser = require("body-parser"),
        graphqlHTTP = require("express-graphql"),
        uuid = require('uuid/v4');

let {
    GraphQLList,
    GraphQLObjectType,
    GraphQLNonNull,
    GraphQLSchema,
    GraphQLError,
    GraphQLString
} = require('graphql');

const {
    TaskType,
    TaskInputType,
    UserType,
    UserInputType
} = require('./models/model')

// get config
let env = process.env.NODE_ENV || 'development'
let config = require('./config')[env]

const port = config.server.port || process.env.PORT || 3000

// prepare app
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// get db utils
let DBUtils = require('./utils/dbUtils')
let db = new DBUtils()

// This is the Root Query
const TasksListQueryRootType = new GraphQLObjectType({
    name: 'TaskListAppSchema',
    description: "Task List Application Schema Root",
    fields: () => ({
        // get all users
        users: {
            type: new GraphQLList(UserType),
            description: "List of all users",
            resolve: resolver.resolver(db.User)
        },
        // get user by id
        user: {
            type: UserType,
            description: "Get user by ID",
            args: {
                id: {
                    type: GraphQLString
                }
            },
            resolve: function (root, { id } ) {
                return db.User.find({
                    where: {
                        id: id
                    }
                }).then(function(user) {
                    return user
                });
            }
        },
        // get all tasks
        tasks: {
            type: new GraphQLList(TaskType),
            description: "List of all tasks",
            resolve: resolver.resolver(db.Task)
        },
        // get task by id
        task: {
            type: TaskType,
            description: "Get task by ID",
            args: {
                id: {
                    type: GraphQLString
                }
            },
            resolve: function (root, { id } ) {
                return db.Task.find({
                    where: {
                        id: id
                    }
                }).then(function(task) {
                    return task
                });
            }
        }
    })
});

// This is the Mutation Root Query
const TasksListMutationRootType = new GraphQLObjectType({
    name: 'TaskListMutationAppSchema',
    description: "Task List Application Schema Root",
    fields: () => ({
        // Mutation for create new task
        newTask: {
            type: TaskType,
            args: {
                task: {
                    type: new GraphQLNonNull(TaskInputType)
                }
            },
            resolve: (root, { task }, info) => {
                return db.User.find({
                    where: {
                        id: task.userId
                    }
                }).then(function(user) {
                    console.log(user)
                    if(user !== undefined && user !== null) {
                        // user exist
                        task.id = uuid.v4();
                        return db.Task.create(task);
                    }
                    else {
                        // error user does not exist
                        throw new GraphQLError("User with id : "+task.userId+" does not exist")
                    }
                });
            }
        },
        // Mutation for create new user
        newUser: {
            type: UserType,
            args: {
                user: {
                    type: new GraphQLNonNull(UserInputType)
                }
            },
            resolve: (root, { user }) => {
                user.id = uuid.v4();
                return db.User.create(user);
            }

        },
        // Mutatioon for delete task
        deleteTask: {
            type: TaskType,
            args: {
                id: {
                    type: new GraphQLNonNull(GraphQLString)
                }
            },
            resolve: (root, { id }) => {
                return db.Task.find({
                    where: {
                        id: id
                    }
                }).then((task) => {
                    return db.Task.destroy({
                        where: {
                            id: id
                        }
                    }).then(function () {
                        return task
                    })
                })
            }
        },
        // Mutatioon for delete user
        deleteUser: {
            type: UserType,
            args: {
                id: {
                    type: new GraphQLNonNull(GraphQLString)
                }
            },
            resolve: (root, { id }) => {
                return db.User.find({
                    where: {
                        id: id
                    }
                }).then((user) => {
                    return db.User.destroy({
                        where: {
                            id: id
                        }
                    }).then(function () {
                        // remove all user tasks
                        return db.Task.destroy({
                            where: {
                                userId: id
                            }
                        }).then(function () {
                            return user
                        })
                    })
                })
            }
        }
    })
});

// GraphQL main schema
const TodoAppSchema = new GraphQLSchema({
    query: TasksListQueryRootType,
    mutation: TasksListMutationRootType
});

// GraphQL with graphic UI
app.use("/graphql", graphqlHTTP( () => (
    {
        schema: TodoAppSchema,
        graphiql: true
    }
    )))


// GraphQL without graphic UI (for app)
app.use("/", graphqlHTTP( () => (
    {
        schema: TodoAppSchema
    }
    )))

app.listen(port)
console.log(`server listening on port ${port}`)