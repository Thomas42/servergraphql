var config = {
    development: {
        url: '',
        // MySQL DB
        database: {
            host:       '',
            port:       '',
            db:         '',
            user:       '',
            password:   ''
        },
        //server details
        server: {
            host: '127.0.0.1',
            port: '8080'
        }
    }
    /*,
    production: {
       url: '',
        // MySQL DB
        database: {
            host:       '',
            port:       '',
            db:         '',
            user:       '',
            password:   ''
        },
        //server details
        server: {
            host: '',
            port: ''
        }
    }*/
};
module.exports = config;