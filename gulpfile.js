const   gulp = require('gulp'),
        nodemon = require('gulp-nodemon');

gulp.task('start', function () {
    nodemon({
        script: 'server.js'
        , env: { 'NODE_ENV': 'development',
                    'PORT': 8080}
    })
})

gulp.task('default', ['start']);